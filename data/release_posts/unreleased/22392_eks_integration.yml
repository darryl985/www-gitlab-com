features:
  primary:
    - name: "AWS EKS integration"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#eks-cluster'
      video: 'https://www.youtube.com/embed/DGvPEJUnXME'
      reporter: danielgruesso
      stage: configure
      issue_url: 'https://gitlab.com/gitlab-org/gitlab/issues/22392'
      description: |
         As popular as Kubernetes is, it was never designed to have a great developer experience.
         Kubernetes is unapologetically complex. Building a cluster from scratch is no easy task.
         Even with managed services like AWS' Elastic Kubernetes Service (EKS) that abstract some
         of the complexity, there are a lot of steps to follow just to get started. Manual steps
         like creating a dedicated Kubernetes control plane and individual nodes in EC are a
         distraction from your main objective to getting code running live. At GitLab we are strong
         believers in automating repetitive tasks and that creating a new cluster should be simple.
         In GitLab 12.5 we've added an EKS integration that does just that.
         
         Now, you can choose EKS as an option from the Gitlab clusters page and you'll be prompted
         to authenticate to your AWS account. Once authenticated, simply specify the desired parameters
         for your cluster and GitLab will take care of the rest.
         
         For us an EKS integration is not just important to allow users to easily deploy to Kuberentes
         on AWS, it's part of our broader multicloud strategy. Together with GitLab's
         [GKE integration](/solutions/google-cloud-platform/) we aim to make not just Kuberentes set up
         easier, but make multicloud easier. And we're just getting started.
         [Our vision](https://about.gitlab.com/direction/configure/kubernetes_management/) is that you'll
         be able to choose from a wide variety of cloud providers for your deployment. You can use each
         provider for the unique strengths it provides and gain portability to move your applications and
         workloads to your cloud of choice, all while maintaining the same consistent workflow in GitLab.
